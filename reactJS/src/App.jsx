import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Conferences from './pages/conferences/Conferences';
import Division from './pages/division/Division';
import Home from './pages/home/Home';
import Diagram from './pages/diagram/Diagram';

import Footer from './components/footer/Footer';
import Header from './components/header/Header';

import Data from './data.json';

import './scss/App.scss';

function App() {
  const data = [...Data];

  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <React.Fragment>
          <Switch>
            <Route path="/afc/north">
              <Division division={data[0].divisions[1]} />
            </Route>
            <Route path="/afc/east">
              <Division division={data[0].divisions[0]} />
            </Route>
            <Route path="/afc/south">
              <Division division={data[0].divisions[2]} />
            </Route>
            <Route path="/afc/west">
              <Division division={data[0].divisions[3]} />
            </Route>
            <Route path="/nfc/north">
              <Division division={data[1].divisions[1]} />
            </Route>
            <Route path="/nfc/east">
              <Division division={data[1].divisions[0]} />
            </Route>
            <Route path="/nfc/south">
              <Division division={data[1].divisions[2]} />
            </Route>
            <Route path="/nfc/west">
              <Division division={data[1].divisions[3]} />
            </Route>
            <Route path="/afc">
              <Conferences data={data[0]} />
            </Route>
            <Route path="/nfc">
              <Conferences data={data[1]} />
            </Route>
            <Route path="/diagram">
              <Diagram />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </React.Fragment>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
