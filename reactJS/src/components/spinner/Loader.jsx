import React from 'react';

import './Loader.scss';

const Loader = () => {
  return (
    <div className="loader center">
      <i className="fas fa-circle-notch fa-spin " />
    </div>
  );
};

export default Loader;
