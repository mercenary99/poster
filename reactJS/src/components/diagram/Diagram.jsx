import React from "react";

import "./Diagram.scss";

const Diagram = ({ team }) => {
  return (
    <section id="diagram">
      <div className="container grid-diagram" id={team}>
        <p className="team-name" />
        <div className="diagram-base" />
        <div className="diagram-won" />
        <div className="diagram-lost" />
      </div>
    </section>
  );
};

export default Diagram;
