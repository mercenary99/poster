import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";

import "./Conference.scss";

const Conference = ({ division, location }) => {
  const path = location.pathname + "/" + division.division.split(" ")[1].toLowerCase();

  return (
    <div className="conference">
      <Link className="grid-item" to={path}>
        <h3 className={"grid-item-title " + location.pathname.split("/")[1]}>{division.division}</h3>
        <div className="grid-container">
          {division.teams.map(t => (
            <div key={t.shortcut} id={t.shortcut} className="grid-img" />
          ))}
        </div>
      </Link>
    </div>
  );
};

export default withRouter(Conference);
