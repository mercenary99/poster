import React from "react";

import "./ByeWeek.scss";

const ByeWeek = ({ teams }) => {
  return (
    <section id="bye-week">
      <div className="container">
        <h2>Bye Week</h2>
        <div className="grid-container">
          {teams.map(t => (
            <div key={t.shortcut} className="grid-item">
              <h3>{t.name}</h3>
              <div id={t.shortcut} className="grid-img" />
              <h3>
                <span>{t.byeWeek}.</span> Week
              </h3>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default ByeWeek;
