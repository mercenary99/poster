import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';

import NFL from '../../assets/logo/nfl.svg';
import NFC from '../../assets/logo/nfc.svg';
import AFC from '../../assets/logo/afc.svg';

import './Header.scss';

const Header = ({ location }) => {
  const [navbar, setNavbar] = useState('navbar top');
  const [confLogo, setConfLogo] = useState(NFL);

  const split = location.pathname.split('/');
  let conference = '';
  let direction = '';
  let division = '';

  /* Navbar transparent on top */
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > window.innerHeight / 2;
      if (show) {
        setNavbar('navbar');
      } else {
        setNavbar('navbar top');
      }
    };
    document.addEventListener('scroll', handleScroll);
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, []);

  /* Set Divison */
  if (split.length === 2) {
    conference = split[1].toUpperCase();
    division = conference;
  } else if (split.length === 3) {
    conference = split[1].toUpperCase();
    direction = split[2].charAt(0).toUpperCase() + split[2].slice(1);
    division = conference + ' ' + direction;
  }

  /* Select the right Logo */
  useEffect(() => {
    switch (split[1]) {
      case 'nfc':
        setConfLogo(NFC);
        break;
      case 'afc':
        setConfLogo(AFC);
        break;
      default:
        setConfLogo(NFL);
        break;
    }
  }, []);

  return (
    <header className="hero" id="header">
      <div className={navbar} id="navbar">
        <div className="container flex-container pt-0">
          <div className="logo">
            <a href="/">
              <img className="conference-logo" src={confLogo} alt="" />
              <h2 className={split[1]}>{division}</h2>
            </a>
          </div>

          <nav className="menu">
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/diagram">Diagram</Link>
              </li>
              <li>
                <Link to="/afc">AFC </Link>
                <i className="fas fa-angle-down"></i>
                <ul>
                  <li>
                    <Link to="/afc/east">AFC East</Link>
                  </li>
                  <li>
                    <Link to="/afc/north">AFC North</Link>
                  </li>
                  <li>
                    <Link to="/afc/south">AFC South</Link>
                  </li>
                  <li>
                    <Link to="/afc/west">AFC West</Link>
                  </li>
                </ul>
              </li>
              <li>
                <Link to="/nfc">NFC </Link>
                <i className="fas fa-angle-down"></i>
                <ul>
                  <li>
                    <Link to="/nfc/east">NFC East</Link>
                  </li>
                  <li>
                    <Link to="/nfc/north">NFC North</Link>
                  </li>
                  <li>
                    <Link to="/nfc/south">NFC South</Link>
                  </li>
                  <li>
                    <Link to="/nfc/west">NFC West</Link>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

      <div className="content">
        <img src={NFL} alt="NFL" />
        <h1>Stats</h1>
        <h2>Season 2020</h2>
        <h3>Week 9</h3>
      </div>
    </header>
  );
};

export default withRouter(Header);
