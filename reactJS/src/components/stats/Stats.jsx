import React from "react";

import FieldGoals from "../../assets/icons/field-goals.png";
import Touchdown from "../../assets/icons/touchdown.png";
import Passing from "../../assets/icons/passing.png";
import Rushing from "../../assets/icons/rushing.png";

import "./Stats.scss";

const Stats = ({ data }) => {
  return (
    <section className="stats" id={data.title}>
      <div className="container flex-container">
        <div className="grid-item">
          {data.title === "Defense" ? (
            ""
          ) : (
            <img src={data.title === "Scoring" ? Touchdown : Rushing} alt="Icon {data.title}" />
          )}
          <h3>{data.left}</h3>
          <h1>{data.leftdata}</h1>
        </div>

        <div className="grid-item grid-title">
          <h2>{data.title}</h2>
          <h3>{data.middle}</h3>
          <h1>{data.middledata}</h1>
        </div>

        <div className="grid-item">
          {data.title === "Defense" ? (
            ""
          ) : (
            <img src={data.title === "Scoring" ? FieldGoals : Passing} alt="Icon {data.title}" />
          )}
          <h3>{data.right}</h3>
          <h1>{data.rightdata}</h1>
        </div>
      </div>
    </section>
  );
};

export default Stats;
