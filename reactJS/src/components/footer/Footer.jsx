import React from "react";

import "./Footer.scss";

const Footer = () => {
  return (
    <footer id="footer">
      <div className="container flex-container">
        <div className="impressum">
          <h4>Impressum</h4>
          <p className="name">Erich Meyer</p>
          <p>Informationsvisualisierung für Ingenieure</p>
          <p>Ostschweizer Fachhochschule</p>
        </div>

        <div className="source">
          <p>
            Data by{" "}
            <a href="https://www.nfl.com/stats/team-stats/" target="_blank" rel="noopener noreferrer">
              NFL 2020 Stats
            </a>
          </p>
          <p>
            Photo by{" "}
            <a
              href="https://unsplash.com/@aussiedave?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText"
              target="_blank"
              rel="noopener noreferrer">
              Dave Adamson
            </a>
            on
            <a
              href="https://unsplash.com/s/photos/american-football?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText"
              target="_blank"
              rel="noopener noreferrer">
              Unsplash
            </a>
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
