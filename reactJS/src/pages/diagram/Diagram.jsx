import React, { Fragment, useState, useEffect } from 'react';
import axios from 'axios';

import Continents from './components/Continents';
import Recap from './components/Recap';

import Loader from '../../components/spinner/Loader';

import './Diagram.scss';

const Diagram = () => {
  // Stats
  const [data, setData] = useState(null);
  const [showLine, setShowLine] = useState(null);
  const [openContinents, setOpenContinents] = useState(null);
  const [continents, setContinents] = useState(null);
  const [countriesObj, setCountriesObj] = useState({
    Africa: [],
    Asia: [],
    Europe: [],
    NorthAmerica: [],
    Oceania: [],
    SouthAmerica: [],
    Global: [],
  });

  // Variables
  let showLineObj = {};

  // Did mount
  useEffect(async () => {
    try {
      const res = await axios.get('https://covid-api.mmediagroup.fr/v1/cases');
      setData(res.data);

      // Save continents in a array
      const contTemp = Object.keys(countriesObj).filter(c => c !== 'Global');
      setContinents([...contTemp]);

      // Set continents to false for checkbox
      let openContTemp = {};
      for (let i = 0; i < contTemp.length; i++) {
        openContTemp = { ...openContTemp, [contTemp[i]]: false };
      }
      setOpenContinents({ ...openContTemp });

      // Save countries (name) i a array
      const countriesTemp = Object.keys(res.data);

      for (let i = 0; i < countriesTemp.length; i++) {
        const currentCountry = countriesTemp[i];

        // Add the appropriate continent to the countries where it is missing
        switch (currentCountry) {
          case 'Cabo Verde':
          case 'Congo (Brazzaville)':
          case 'Congo (Kinshasa)':
          case 'Eswatini':
            res.data[currentCountry].All.continent = 'Africa';
            break;
          case 'Burma':
          case 'Sri Lanka':
          case 'Taiwan*':
          case 'Timor-Leste':
          case 'West Bank and Gaza':
            res.data[currentCountry].All.continent = 'Asia';
            break;
          case 'Kosovo':
          case 'Montenegro':
          case 'Serbia':
            res.data[currentCountry].All.continent = 'Europe';
            break;
          case 'Micronesia':
            res.data[currentCountry].All.continent = 'Oceania';
            break;
          default:
            break;
        }

        // Save countries in the right continents
        switch (res.data[currentCountry].All.continent) {
          case 'Africa':
            countriesObj.Africa.push(currentCountry);
            break;
          case 'Asia':
            countriesObj.Asia.push(currentCountry);
            break;
          case 'Europe':
            countriesObj.Europe.push(currentCountry);
            break;
          case 'North America':
            countriesObj.NorthAmerica.push(currentCountry);
            break;
          case 'Oceania':
            countriesObj.Oceania.push(currentCountry);
            break;
          case 'South America':
            countriesObj.SouthAmerica.push(currentCountry);
            break;
          default:
            break;
        }

        // Set countries to false for checkbox
        showLineObj = { ...showLineObj, [currentCountry]: false };
      }

      // Set 5 countries to true to see them at the beginning
      showLineObj = { ...showLineObj, Austria: true, France: true, Germany: true, Italy: true, Switzerland: true };
      setShowLine({ ...showLineObj });
    } catch (err) {
      console.error(err);
      return <h1 className="center">ERROR</h1>;
    }
  }, []);

  // Waiting that all stats are ready
  if (!data || !openContinents || !setContinents || !showLine) return <Loader />;

  // Functions for the children
  const showLineHandlers = e => {
    setShowLine({ ...showLine, [e]: !showLine[e] });
  };
  const openContinentsHandlers = e => {
    setOpenContinents({ ...openContinents, [e]: !openContinents[e] });
  };

  // Find all countries in stats where are true
  const findTrue = obj => {
    let trueCountries = [];
    for (let o in obj) {
      if (obj[o] === true) {
        trueCountries.push(o);
      }
    }
    return trueCountries;
  };

  return (
    <Fragment>
      <div className="flex-container">
        <h1>Corona Stats World Wide</h1>
      </div>
      <ul>
        {continents.map(c => (
          <Continents
            key={c}
            continent={c}
            countries={countriesObj}
            showLine={showLine}
            showLineHandlers={showLineHandlers}
            openContinents={openContinents}
            openContinentsHandlers={openContinentsHandlers}
          />
        ))}
      </ul>
      <div className="container">
        {findTrue(showLine)
          .sort()
          .map(c => (
            <Recap key={c} delivery={data[c].All} />
          ))}
      </div>
    </Fragment>
  );
};

export default Diagram;
