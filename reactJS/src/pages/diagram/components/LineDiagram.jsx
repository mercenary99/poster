import React from 'react';
import useAxios from 'axios-hooks';
import { VictoryChart, VictoryLine, VictoryAxis, createContainer } from 'victory';

import Loader from '../../../components/spinner/Loader';

const VictoryVoronoiContainer = createContainer('voronoi');

const LineDiagram = ({ delivery }) => {
  const [{ data: conData, loading: conLoad, error: conErr }] = useAxios(
    `https://covid-api.mmediagroup.fr/v1/history?country=${delivery.country}&status=Confirmed`
  );
  const [{ data: deaData, loading: deaLoad, error: deaErr }] = useAxios(
    `https://covid-api.mmediagroup.fr/v1/history?country=${delivery.country}&status=Deaths`
  );
  const [{ data: recData, loading: recLoad, error: recErr }] = useAxios(
    `https://covid-api.mmediagroup.fr/v1/history?country=${delivery.country}&status=Recovered`
  );

  if (conLoad || deaLoad || recLoad) return <Loader />;
  if (conErr || deaErr || recErr) return <h1>ERROR!</h1>;

  const confirmed = [];
  const deaths = [];
  const recovered = [];

  for (let i = 0; i < 30; i++) {
    confirmed.push({
      x: Object.keys(conData.All.dates)[i].split('-').reverse().join('.'),
      y: Object.values(conData.All.dates)[i],
    });
    deaths.push({
      x: Object.keys(deaData.All.dates)[i].split('-').reverse().join('.'),
      y: Object.values(deaData.All.dates)[i],
    });
    recovered.push({
      x: Object.keys(recData.All.dates)[i].split('-').reverse().join('.'),
      y: Object.values(recData.All.dates)[i],
    });
  }

  return (
    <div className="diagram-item line-diagram">
      <h4>Timelline</h4>
      <VictoryChart
        padding={{ top: 10, right: 10, bottom: 50, left: 60 }}
        domainPadding={{ y: 50 }}
        minDomain={{ y: 0 }}
        containerComponent={
          <VictoryVoronoiContainer
            voronoiDimension="x"
            labels={({ datum }) => `x: ${datum.x}
                                    y: ${datum.y}`}
          />
        }>
        <VictoryAxis
          style={{
            axis: { stroke: '#000' },
            axisLabel: { fontSize: 10 },
            ticks: { stroke: '#000' },
            grid: { stroke: '#000', strokeWidth: 0.25 },
            tickLabels: { fontSize: 10, padding: 2, textAnchor: 'end' },
          }}
          dependentAxis
        />
        <VictoryAxis
          style={{
            axis: { stroke: '#000' },
            axisLabel: { fontSize: 8 },
            ticks: { stroke: '#000' },
            grid: { stroke: '#999', strokeWidth: 0.25 },
            tickLabels: { fontSize: 8, padding: 1, angle: -45, textAnchor: 'end' },
          }}
        />
        <VictoryLine style={{ data: { stroke: '#db2027' }, labels: { fill: '#db2027' } }} data={confirmed.reverse()} />
        <VictoryLine style={{ data: { stroke: '#222' }, labels: { fill: '#222' } }} data={deaths.reverse()} />
        <VictoryLine style={{ data: { stroke: '#003c66' }, labels: { fill: '#003c66' } }} data={recovered.reverse()} />
      </VictoryChart>
    </div>
  );
};

export default LineDiagram;
