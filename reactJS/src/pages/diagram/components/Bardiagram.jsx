import React from 'react';
import { VictoryChart, VictoryAxis, VictoryBar, VictoryTooltip } from 'victory';

const Bardiagram = ({ delivery }) => {
  return (
    <div className="diagram-item">
      <h4>Total</h4>
      <VictoryChart padding={{ top: 10, bottom: 50, left: 90 }} domainPadding={{ x: 40 }} width={300}>
        <VictoryAxis
          style={{
            axis: { stroke: '#000' },
            axisLabel: { fontSize: 12 },
            ticks: { stroke: '#000' },
            grid: { stroke: '#000', strokeWidth: 0.25 },
            tickLabels: { fontSize: 12, padding: 5, textAnchor: 'end' },
          }}
          dependentAxis
        />
        <VictoryAxis
          style={{
            axis: { stroke: '#000' },
            axisLabel: { fontSize: 12 },
            ticks: { stroke: '#000' },
            grid: { stroke: '#999', strokeWidth: 0.25 },
            tickLabels: { fontSize: 12, padding: 5, angle: -30, textAnchor: 'end' },
          }}
        />

        <VictoryBar
          labelComponent={
            <VictoryTooltip
              style={{ fontSize: '16px' }}
              cornerRadius={5}
              pointerLength={10}
              flyoutStyle={{
                stroke: 'grey',
                strokeWidth: 1,
              }}
              flyoutPadding={{
                top: 5,
                right: 10,
                bottom: 5,
                left: 10,
              }}
            />
          }
          style={{
            data: {
              fill: ({ datum }) => datum.fill,
            },
          }}
          barRatio={1}
          data={[
            { x: 'confirmed', y: delivery.confirmed, fill: '#db2027', label: delivery.confirmed },
            { x: 'recovered', y: delivery.recovered, fill: '#003c66', label: delivery.recovered },
            { x: 'deaths', y: delivery.deaths, fill: '#222', label: delivery.deaths },
          ]}
          events={[
            {
              target: 'data',
              eventHandlers: {
                onMouseOver: () => {
                  return [
                    {
                      target: 'data',
                      mutation: () => ({ style: { fill: '#FAB101' } }),
                    },
                    {
                      target: 'labels',
                      mutation: () => ({ active: true }),
                    },
                  ];
                },
                onMouseOut: () => {
                  return [
                    {
                      target: 'data',
                      mutation: () => {},
                    },
                    {
                      target: 'labels',
                      mutation: () => ({ active: false }),
                    },
                  ];
                },
              },
            },
          ]}
        />
      </VictoryChart>
    </div>
  );
};

export default Bardiagram;
