import React, { Fragment } from 'react';

const Countries = ({ country, showLine, showLineHandlers }) => {
  return (
    <Fragment>
      {country.map(c => (
        <li key={c} className="country-item">
          <label className="label">
            {c}
            <input type="checkbox" id={c} name={c} checked={showLine[c]} onChange={() => showLineHandlers(c)} />
            <span className="checkmark"></span>
          </label>
        </li>
      ))}
    </Fragment>
  );
};

export default Countries;
