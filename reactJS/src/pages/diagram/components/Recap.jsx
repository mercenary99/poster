import React from 'react';

import Bardiagram from './Bardiagram';
import LineDiagram from './LineDiagram';

const formatNumber = x => {
  let a = x.toString().split('').reverse();
  let r = '';

  for (let i = 0; i < a.length; i++) {
    switch (i) {
      case 0:
        r = a[i];
        break;
      case 1:
      case 2:
      case 4:
      case 5:
      case 7:
      case 8:
      case 10:
      case 11:
      case 13:
      case 14:
      case 16:
      case 17:
        r = r + a[i];
        break;
      case 3:
      case 6:
      case 9:
      case 12:
      case 15:
      case 18:
        r = r + ',' + a[i];
        break;
      default:
        console.log('More then 18 digits');
        break;
    }
  }
  return r.split('').reverse().join('');
};

const Recap = ({ delivery }) => {
  return (
    <div className="recap">
      <h3 className="country">{delivery.country}</h3>
      <p>Population: {formatNumber(delivery.population)}</p>
      <br />
      <div className="flex-container">
        <Bardiagram delivery={delivery} />
        <LineDiagram delivery={delivery} />
      </div>
    </div>
  );
};

export default Recap;
