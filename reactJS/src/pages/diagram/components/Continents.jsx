import React from 'react';

import Countries from './Countries';

const Continents = ({ continent, countries, showLine, showLineHandlers, openContinents, openContinentsHandlers }) => {
  return (
    <form>
      <li className="continent-item" key={continent + '-li'}>
        <label className="label">
          {continent}
          <input
            type="checkbox"
            id={continent}
            className="continent-input"
            name={continent}
            checked={openContinents[continent]}
            onChange={() => openContinentsHandlers(continent)}
          />
          <span className="checkmark arrow"></span>
        </label>
        <ul
          className={openContinents[continent] ? 'country-list country-list-checked' : 'country-list'}
          id={continent + '-ul'}>
          <Countries
            continent={continent}
            key={countries[0]}
            country={countries[continent.split(' ').join('')]}
            showLine={showLine}
            showLineHandlers={showLineHandlers}
          />
        </ul>
      </li>
    </form>
  );
};

export default Continents;
