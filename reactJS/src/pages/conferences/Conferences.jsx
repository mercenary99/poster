import React from "react";
import { withRouter } from "react-router";

import Conference from "../../components/conference/Conference";

import "./Conferences.scss";

const Conferences = ({ data, location }) => {
  return (
    <main id="conferences">
      <div className={"container  " + data.conference}>
        <h1>{data.name}</h1>
        <h1>{data.conference}</h1>
        <div className="grid-container">
          {data.divisions.map(d => (
            <Conference key={d.id} division={d} />
          ))}
        </div>
      </div>
    </main>
  );
};

export default withRouter(Conferences);
