import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";

import AFC from "../../assets/logo/afc.svg";
import NFC from "../../assets/logo/nfc.svg";

import "./Home.scss";

const Home = () => {
  return (
    <main id="home">
      <div className="container">
        <h1>Welcome to the NFL Stats page</h1>
        <div className="flex-container">
          <Link to="/afc">
            <img src={AFC} alt="AFC" />
            <h2 className="color-red">American Football Conference</h2>
            <h2 className="color-red">AFC</h2>
          </Link>

          <Link to="/nfc">
            <img src={NFC} alt="NFC" />
            <h2 className="color-blue">National Football Conference</h2>
            <h2 className="color-blue">NFC</h2>
          </Link>
        </div>
      </div>
    </main>
  );
};

export default withRouter(Home);
