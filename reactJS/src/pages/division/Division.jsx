import React from "react";

import ByeWeek from "../../components/byeWeek/ByeWeek";
import Diagram from "../../components/diagram/Diagram";
import Stats from "../../components/stats/Stats";

import "./Division.scss";

const Division = ({ division }) => {
  return (
    <main id="division">
      <h1>{division.division}</h1>
      {division.stats.map(s => (
        <Stats key={s.id} data={s} />
      ))}
      {division.teams.map(t => (
        <Diagram key={t.shortcut} team={t.shortcut} />
      ))}
      <ByeWeek key={division.id} teams={division.teams} />
    </main>
  );
};

export default Division;
