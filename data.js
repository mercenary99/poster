const data = [
  {
    'id': 1,
    'conference': 'AFC',
    'name': 'American Football Conference',
    'divisions': [
      { 
        'id': 1,
        'division': 'AFC East',
        'teams': [
          {
            'shortcut': 'BUF',
            'name': 'Buffalo Bils',
            'img': '/static/media/BUF.80e75631.svg',
            'byeWeek': '11'
          },
          {
            'shortcut': 'MIA',
            'name': 'Miami Dolphins',
            'img': '/static/media/MIA.217ca1dc.svg',
            'byeWeek': '7'
          },
          {
            'shortcut': 'NE',
            'name': 'New England Patriots',
            'img': '/static/media/NE.918773be.svg',
            'byeWeek': '5'
          },
          {
            'shortcut': 'NYJ',
            'name': 'New York Jets',
            'img': '/static/media/NYJ.ae342b39.svg',
            'byeWeek': '10'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '81',
            'rightdata': '62'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '3847',
            'rightdata': '7907'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '75',
            'middledata': '30',
            'rightdata': '30'
          }
        ] 
      },
      {
        'id': 2,
        'division': 'AFC North',
        'teams': [
          {
            'shortcut': 'PIT',
            'name': 'Pittsburgh Steelers',
            'img': '/static/media/PIT.d5d5a644.svg',
            'byeWeek': '4'
          },
          {
            'shortcut': 'BAL',
            'name': 'Baltimore Ravens',
            'img': '/static/media/BAL.cb83ac96.svg',
            'byeWeek': '7'
          },
          {
            'shortcut': 'CLE',
            'name': 'Cleveland Browns',
            'img': '/static/media/CLE.081ce508.svg',
            'byeWeek': '9'
          },
          {
            'shortcut': 'CIN',
            'name': 'Cincinnati Bengals',
            'img': '/static/media/CIN.c9f599bf.svg',
            'byeWeek': '9'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '100',
            'rightdata': '54'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '4243',
            'rightdata': '7402'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '70',
            'middledata': '30',
            'rightdata': '33'
          }
        ] 
      },
      {
        'id': 3,
        'division': 'AFC South',
        'teams': [
          {
            'shortcut': 'TEN',
            'name': 'Tennessee Titans',
            'img': '/static/media/TEN.d7c2fea3.svg',
            'byeWeek': '4'
          },
          {
            'shortcut': 'IND',
            'name': 'Indianapilis Colts',
            'img': '/static/media/IND.f1fffc05.svg',
            'byeWeek': '7'
          },
          {
            'shortcut': 'HOU',
            'name': 'Houston Texans',
            'img': '/static/media/HOU.798b8139.svg',
            'byeWeek': '8'
          },
          {
            'shortcut': 'JAX',
            'name': 'Jacksonwille Jaguars',
            'img': '/static/media/JAX.2109c845.svg',
            'byeWeek': '8'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '95',
            'rightdata': '52'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '3492',
            'rightdata': '8620'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '69',
            'middledata': '28',
            'rightdata': '21'
          }
        ] 
      },
      {
        'id': 4,
        'division': 'AFC West',
        'teams': [
          {
            'shortcut': 'KC',
            'name': 'Kansas City Chiefs',
            'img': '/static/media/KC.f6e5e770.svg',
            'byeWeek': '10'
          },
          {
            'shortcut': 'LV',
            'name': 'Las Vegas Raiders',
            'img': '/static/media/LV.1ea2fb8c.svg',
            'byeWeek': '6'
          },
          {
            'shortcut': 'DEN',
            'name': 'Denver Broncos',
            'img': '/static/media/DEN.a24f1f99.svg',
            'byeWeek': '5'
          },
          {
            'shortcut': 'LAC',
            'name': 'Los Angeles Chargers',
            'img': '/static/media/LAC.df461cbe.svg',
            'byeWeek': '6'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '101',
            'rightdata': '62'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '4057',
            'rightdata': '9057'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '64',
            'middledata': '22',
            'rightdata': '21'
          }
        ] 
      }
    ]
  },
  {
    'id': 2,
    'conference': 'NFC',
    'name': 'National Football Conference',
    'divisions': [
      {
        'id': 1,
        'division': 'NFC East',
        'teams': [
          {
            'shortcut': 'PHI',
            'name': 'Philadelphia Eagles',
            'img': '/static/media/PHI.ca3e56e3.svg',
            'byeWeek': '9'
          },
          {
            'shortcut': 'WAS',
            'name': 'Washington Football Team',
            'img': '/static/media/WAS.f1859c0a.svg',
            'byeWeek': '8'
          },
          {
            'shortcut': 'DAL',
            'name': 'Dallas Cowboys',
            'img': '/static/media/DAL.527c2bec.svg',
            'byeWeek': '10'
          },
          {
            'shortcut': 'NYG',
            'name': 'New York Giants',
            'img': '/static/media/NYG.6212e35b.svg',
            'byeWeek': '11'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '79',
            'rightdata': '54'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '3631',
            'rightdata': '8498'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '114',
            'middledata': '22',
            'rightdata': '31'
          }
        ] 
      },
      {
        'id': 2,
        'division': 'NFC North',
        'teams': [
          {
            'shortcut': 'GB',
            'name': 'Green Bay Packers',
            'img': '/static/media/GB.adbe0a99.svg',
            'byeWeek': '5'
          },
          {
            'shortcut': 'CHI',
            'name': 'Chicago Bears',
            'img': '/static/media/CHI.86760d0d.svg',
            'byeWeek': '11'
          },
          {
            'shortcut': 'MIN',
            'name': 'Minnesota Vikings',
            'img': '/static/media/MIN.a0759ab1.svg',
            'byeWeek': '7'
          },
          {
            'shortcut': 'DET',
            'name': 'Detroit Lions',
            'img': '/static/media/DET.14277ce8.svg',
            'byeWeek': '5'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '100',
            'rightdata': '46'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '3846',
            'rightdata': '8635'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '70',
            'middledata': '19',
            'rightdata': '30'
          }
        ] 
      },
      {
        'id': 3,
        'division': 'NFC South',
        'teams': [
          {
            'shortcut': 'NO',
            'name': 'New Orleans Saints',
            'img': '/static/media/NO.04c15ba2.svg',
            'byeWeek': '6'
          },
          {
            'shortcut': 'TB',
            'name': 'Tampa Bay Buccaneers',
            'img': '/static/media/TB.b1982017.svg',
            'byeWeek': '13'
          },
          {
            'shortcut': 'ATL',
            'name': 'Atlanta Falcons',
            'img': '/static/media/ATL.3a2d3960.svg',
            'byeWeek': '10'
          },
          {
            'shortcut': 'CAR',
            'name': 'Carolina Panthers',
            'img': '/static/media/CAR.e3721488.svg',
            'byeWeek': '13'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '105',
            'rightdata': '73'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '3752',
            'rightdata': '9809'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '60',
            'middledata': '30',
            'rightdata': '24'
          }
        ] 
      },
      {
        'id': 4,
        'division': 'NFC West',
        'teams': [
          {
            'shortcut': 'SEA',
            'name': 'Seattle Seahawks',
            'img': '/static/media/SEA.7d82ebce.svg',
            'byeWeek': '6'
          },
          {
            'shortcut': 'ARI',
            'name': 'Arizona Cardinals',
            'img': '/static/media/ARI.2e0f5228.svg',
            'byeWeek': '8'
          },
          {
            'shortcut': 'LA',
            'name': 'Los Angeles Rams',
            'img': '/static/media/LA.895ea623.svg',
            'byeWeek': '9'
          },
          {
            'shortcut': 'SF',
            'name': 'San Francisco 49ers',
            'img': '/static/media/SF.c05a2643.svg',
            'byeWeek': '11'
          }
        ],
        'stats': [
          {
            'id': 1,
            'title': 'Scoring',
            'left': 'Total Touchdowns',
            'right': 'Total Field Goals',
            'leftdata': '117',
            'rightdata': '38'
          },
          {
            'id': 2,
            'title': 'Offense',
            'left': 'Total Rushing Yards',
            'right': 'Total Passing Yards',
            'leftdata': '4411',
            'rightdata': '9287'
          },
          {
            'id': 3,
            'title': 'Defense',
            'left': 'Total Sacks',
            'middle': 'Total Interceptions',
            'right': 'Total Fumbles',
            'leftdata': '67',
            'middledata': '29',
            'rightdata': '25'
          }
        ] 
      }
    ]
  },
]

export default data;